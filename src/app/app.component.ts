import { Component } from '@angular/core';
import { SearchService } from './search.service';
import { AlbumModel } from './album.model';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  albums: AlbumModel[] = [];

  constructor(private service: SearchService) {
  }

  uniq(albums: AlbumModel[], param: string) {
    return albums.filter((item, pos, array) => {
      return array.map(mapItem => mapItem[param]).indexOf(item[param]) === pos;
    });
  }

  searchAlbums(artist: string) {
    this.albums = [];
    this.service.getItunesData(artist).subscribe(res => console.log(res));
    const modifiedArtist = artist.split(' ').join('+');
    forkJoin(this.service.getItunesData(modifiedArtist), this.service.getDeezerData(modifiedArtist))
      .subscribe(result => {
          result[0].results.map( (album) => {
            this.albums.push({
              title: album.collectionName,
              albumImage: album.artworkUrl100,
              tracksCount: album.trackCount,
              link: album.collectionViewUrl,
              artistName: album.artistName
            });
          });

          result[1].data.map( (album) => {
              this.albums.push({
                title: album.title,
                albumImage: album.cover_big,
                tracksCount: album.nb_tracks,
                link: album.link,
                artistName: album.artist.name,
              });
            }
          );
          this.albums = this.uniq(this.albums, 'title');
        }
      );
  }
}
