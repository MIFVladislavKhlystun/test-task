import {Artist} from './artist';

export interface Deezer {
      data: any[];
      artist: Artist;
      title: string;
      cover_big: string;
      nb_tracks: number;
      link: string;
}
