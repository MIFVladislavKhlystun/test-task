export interface Itunes {
  results: any[];
  artistName: string;
  collectionName: string;
  artworkUrl100: string;
  trackCount: number;
  collectionViewUrl: string;
}
