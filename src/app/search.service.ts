import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Deezer } from './deezer';
import { Itunes } from './itunes';


@Injectable({
  providedIn: 'root'
})

export class SearchService {

  constructor(private http: HttpClient) { }

  getDeezerData(artist: string) {
    return this.http.jsonp<Deezer>(`https://api.deezer.com/search/album/?q=${artist}&output=jsonp`, 'callback');
  }

  getItunesData(artist: string) {
    return this.http.get<Itunes>(`https://itunes.apple.com/search?term=${artist}&entity=album`);
  }
}
